$(document).ready(function() {
  var account = {
    '1234' : {'pin': '1234', 'balance': 500},
    '5678' : {'pin': '5678', 'balance': 1500},
    '0000' : {'pin': '0000', 'balance': 2500}
  };

  $('.form-menu').hide();
  $('#displaybalance').hide();

  $('.form-signin').submit(function() {
    var accountNo = $('#accountNo').val();
    var pin = $('#pin').val();
    if (accountNo != null && account[accountNo]['pin'] == pin) {
      $('.form-menu').show();
      $('.form-signin').hide();
      $('#displaybalance').show();
			$('#a').hide();
			$('#b').hide();
      showbalance(accountNo);
      $('#o').click(function(){
				var num = parseInt($('#p').val());
				if (isNaN(num)){
					alert("กรุณาป้อนข้อมูล !");
				} else {
          if (num <= 0){
            alert('กรุณากรอกข้อมูลใหม่');
          } else{
					deposit(accountNo, num);
				}
        }
			});

      $('#g').click(function(){
				var num = parseInt($('#k').val());
				if (isNaN(num)){
					alert("กรุณาป้อนข้อมูล !");
				} else {
          if (num <= 0 || num> account[accountNo]['balance']){
            alert('กรุณากรอกข้อมูลใหม่');
          } else{
					withdraw(accountNo, num);
				}
        }
			});
    } else {
      alert('เลขที่บัญชีหรือรหัสประจำตัวไม่ถูกต้อง');
    }
    return false;
  })
  function showbalance(accountNo) {
    $("#balance").html(account[accountNo]['balance'].toString())
  }
  function deposit(accountNo, amount) {
    account[accountNo]['balance'] += amount
alert("ฝากสำเร็จ !");
    showbalance(accountNo);
  

  }

  function withdraw(accountNo, amount) {
    account[accountNo]['balance'] -= amount
alert("ถอนสำเร็จ !");
    showbalance(accountNo);

  }
	
  $('#mu').click(function(){
		$('#a').toggle(); 
	});

  $('#tu').click(function(){
		$('#b').toggle();
	});
})
